package ru.alfabank.plugin

import org.gradle.api.Project
import org.gradle.api.Plugin

class MicroserviceTemplatePlugin implements Plugin<Project> {

    void apply(Project project) {
        project.task('updateGradleFiles') << {
            File tmp = new File("./tmp")
            tmp.mkdirs()
            "git init".execute(null, new File("./tmp"))
            def gitPull = "git pull https://andreythegreatest@bitbucket.org/andreythegreatest/microtemplate.git"
            Process process = gitPull.execute(null, new File("./tmp")) 
            process.waitFor()

            project.copy {
                from "./tmp" + '/resources/gradle'
                into './'
            }

            tmp.deleteDir()
        }
    }
}