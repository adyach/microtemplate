import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import net.logstash.logback.appender.LogstashTcpSocketAppender
import net.logstash.logback.encoder.LogstashEncoder
import static ch.qos.logback.classic.Level.*

def appenderList = ["CONSOLE", "FILE", "logstash"]
def appInstanceName = System.getenv("SERVICE_ID")
def profile = System.getenv("SPRING_PROFILES_ACTIVE")
def serviceHost = System.getenv("SERVICE_HOST")

jmxConfigurator()

println "="*80
println """
    APP INSTANCE NAME : $appInstanceName
    APP PROFILE       : $profile
    SERVICE HOST      : $serviceHost
    LOGGING FILE      : logback.groovy
"""
println "="*80

logger('org.springframework', OFF)
logger('org.springframework.boot', OFF)
logger('org.springframework.web.servlet', OFF)
logger('org.springframework.security.web', OFF)
logger('org.springframework.context.support', OFF)
logger("ru.alfabank.authentication", INFO)

appender("CONSOLE", ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%-4relative %d %-5level [ %t ] %-55logger{13} | %m %n"
    }
}

if (null != serviceHost) {
    appender("logstash", LogstashTcpSocketAppender) {
        remoteHost = serviceHost
        port = 5959
        encoder(LogstashEncoder)
    }
}
appender("FILE", RollingFileAppender) {
    file = "logfile.log"
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "logfile.%d{yyyy-MM-dd}.log"
        maxHistory = 30
    }
    encoder(PatternLayoutEncoder) {
        pattern = "%-4relative [%thread] %-5level %logger{35} - %msg%n"
    }
}

org.slf4j.MDC.put("app_id", appInstanceName)
root(INFO, appenderList)